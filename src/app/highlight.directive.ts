import { Directive, ElementRef, Input, OnChanges, SimpleChanges } from '@angular/core';
import { TodoListService } from './services/todo-list.service';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective implements OnChanges {

  @Input() text = '';

  private readonly kreopoleioList = ['κοτόπουλο', 'κοτοπουλο', 'κιμάς', 'κιμας', 'κιλότο', 'κιλοτο'];
  private readonly laikiList = ['αγγούρια', 'αγγουρια', 'ντομάτες', 'ντοματες', 'κολοκύθια', 'κολοκυθια', 'καρότα', 'καροτα', 'πατάτες', 'πατατες', 'πορτοκάλια', 'πορτοκαλια'];

  constructor(private el: ElementRef, private todoListService: TodoListService) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.el.nativeElement.style.backgroundColor = this.getColor();
  }

  getColor(): string {
    if (!this.text || this.text === '') {
      return '';
    }

    if (this.todoListService.mamaList.some(mamaItem => this.text.includes(mamaItem))) {
      return '#972c0757';
    }

    if (this.todoListService.laikiList.some(laikiItem => this.text.includes(laikiItem))) {
      return '#07971c57';
    }

    if (this.todoListService.kreopoleioList.some(kreopoleioItem => this.text.includes(kreopoleioItem))) {
      return '#07449757';
    }
  }

}
