import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map, debounceTime } from 'rxjs/operators';
import { TodoListService } from './services/todo-list.service';
import { createTodoItem, TodoItem } from './todo-list';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  public itemControl = new FormControl('', [Validators.required, Validators.minLength(3)]);
  public filteredItems: Observable<TodoItem[]>;
  public itemToEdit: TodoItem;

  private _todoItems: TodoItem[] = [];

  constructor(private todoListService: TodoListService) { }

  public get todoItems(): TodoItem[] {
    return this._todoItems.filter(item => !item.deleted);
  }

  public async ngOnInit(): Promise<void> {
    await this.todoListService.predefinedLists();
    this.todoListService.items.subscribe(items => {
      this._todoItems = items;
    });

    this.filteredItems = this.itemControl.valueChanges
      .pipe(
        debounceTime(200),
        startWith(''),
        map(value => value?.length > 1 ? this._filter(value) : [])
      );
  }

  public saveItem(): void {
    if (this.itemControl.valid) {
      const newItem = createTodoItem(this.itemControl.value);
      if (this.itemToEdit) {
        newItem.id = this.itemToEdit.id;
        this.todoListService.updateItem(newItem);
        this.itemToEdit = null;
      } else {
        this.todoListService.saveItem(newItem);
      }
      this.itemControl.reset();
    }
  }

  public onSelectItem(selectedItem: TodoItem): void {
    selectedItem.checked = !selectedItem.checked;
    this.todoListService.checkItem(selectedItem);
  }

  public onDeleteItem(itemToDelete: TodoItem): void {
    itemToDelete.deleted = true;
    this.todoListService.deleteItem(itemToDelete);
  }

  public onEditItem(item: TodoItem): void {
    this.itemToEdit = item;
    this.itemControl.setValue(item.itemName);
  }

  public reset(): void {
    this.itemToEdit = null;
    this.itemControl.setValue('');
    this.itemControl.markAsUntouched();
  }

  private _filter(value: string): TodoItem[] {
    const filterValue = value.toLowerCase();
    const loadedItems = [];

    return this._todoItems.filter(option => {
      const optionLowerCase = option.itemName.toLowerCase();
      if (!loadedItems.includes(optionLowerCase) && optionLowerCase.includes(filterValue)) {
        loadedItems.push(optionLowerCase);
        return true;
      }

      return false;
    });
  }
}
