export interface TodoItem {
    id?: string;
    itemName: string;
    checked: boolean;
    deleted: boolean;
}

export function createTodoItem(itemName: string): TodoItem {
    if (itemName === '') {
        return null;
    }

    return {
        itemName,
        checked: false,
        deleted: false
    };
}
