import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { combineLatest, from, Observable } from 'rxjs';
import { TodoItem } from '../todo-list';

@Injectable({
  providedIn: 'root'
})
export class TodoListService {
  mamaList = [];
  laikiList = [];
  kreopoleioList = [];
  private _db = firebase.firestore();

  constructor(private _firestore: AngularFirestore) {
  }

  public get items(): Observable<TodoItem[]> {
    return this._firestore.collection<TodoItem>('todoItems').valueChanges();
  }

  public async predefinedLists(): Promise<void> {
    return new Promise((resolve) => {
      combineLatest([
        this._firestore.collection<{ [key: string]: string }>('mamaList').valueChanges(),
        this._firestore.collection<{ [key: string]: string }>('laikiList').valueChanges(),
        this._firestore.collection<{ [key: string]: string }>('kreopoleioList').valueChanges()
      ]).subscribe(([mama, laiki, kreopoleio]) => {
        mama.forEach(item => this.mamaList.push(...Object.values(item)));
        laiki.forEach(item => this.laikiList.push(...Object.values(item)));
        kreopoleio.forEach(item => this.kreopoleioList.push(...Object.values(item)));
        resolve();
      });
    });
  }

  public saveItem(item: TodoItem): void {
    this._db.collection('todoItems').add(item)
      .then(addedItem => {
        this._db.collection('todoItems').doc(addedItem.id).update({ id: addedItem.id });
        console.log('Item added with ID: ', addedItem.id);
      })
      .catch(error => {
        console.error('Error adding document: ', error);
      });
  }

  public updateItem(item: TodoItem): void {
    this._db.collection('todoItems').doc(item.id).update({ itemName: item.itemName })
      .catch(error => {
        console.error('Error updating document: ', error);
      });
  }

  public deleteItem(item: TodoItem): void {
    this._db.collection('todoItems').doc(item.id).update({ deleted: item.deleted })
      .catch(error => {
        console.error('Error adding document: ', error);
      });
  }

  public checkItem(item: TodoItem): void {
    this._db.collection('todoItems').doc(item.id).update({ checked: item.checked })
      .catch(error => {
        console.error('Error adding document: ', error);
      });
  }
}
